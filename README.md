# README #
Kaliteli sosyal içerik ve bilgi platformları arasında yerini almayı başaran GecBunlari, 2015 senesinden bu yana faaliyetlerine devam ediyor. O günden bu yana doğru ve güncel bilgiyi kullanıcılarla buluşturmayı hedefleyen platform, birçok kategoride özgün içerikler yayınlıyor.

[Teknoloji Haberleri](https://gecbunlari.com/)
